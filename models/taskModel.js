const mongoose = require('mongoose');

const taskSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, "Place add a task"]
        },
        completed:{
                type: Boolean,
                required: true,
                default: false,
        }
    },
    {
        timestamps:true,
    }
);


const Taks = mongoose.model('Task', taskSchema );

module.exports = Taks