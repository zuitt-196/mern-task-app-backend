const dotenv = require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose');
const taskRoutes = require('./routes/taskRoutes.js');
const cors = require('cors');

mongoose.set('strictQuery', false);

const app = express();
const PORT = process.env.PORT || 5000


// ============ THE SECOND METHOD ============ 
mongoose.connect(process.env.MONGO_URI).then(()=>{
    app.listen(PORT, () =>{
        console.log(`Server running  on PORT  ${PORT}`);
    })
}).catch((error) => console.log(error));

//front end- git@gitlab.com:zuitt-196/mern-taak-app-frontend.git

// backend = git@gitlab.com:zuitt-196/mern-task-app-backend.git

//  MIDDLWARE

app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.use(cors(
    {
        origing: ["http://localhost:3000/", ]
    }
))
app.use("/api/tasks", taskRoutes);

// MIDDLEWARE CONNECTION OF FRONTEND 

//ROUTES [SECTION]
app.get('/', (req, res) => {    
    res.send("Home page")
})
   




// ============ THE FIRT METHOD L===================
// SERVER [SECTION] CONNECION WITH USE OF MONGOOES OR THE  CONNECTIION OF DATABASE
// const startServer = async () => {
//         try {
//             await connectDB();
//             app.listen(PORT, () =>{
//                 console.log(`Server running  on PORT  ${PORT}`);
//         })
        
//         } catch (erorr) {
//             console.log(error)
//         }
// }
// startServer()